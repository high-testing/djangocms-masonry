# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
from django.db import models
from django.utils.translation import ugettext as _

from cms.models import CMSPlugin
from jsonfield import JSONField
from django.core.validators import MaxValueValidator, MinValueValidator

from .conf import settings

import logging
logger = logging.getLogger(__name__)

class AbstractMasonryBase(CMSPlugin):
    extra_options = JSONField(_('JSON options'), blank=True, default={})

    class Meta:
        abstract = True


class Masonry(AbstractMasonryBase):

    column_size = models.PositiveSmallIntegerField(
        default=30,
        validators=[
            MaxValueValidator(100),
            MinValueValidator(1),
        ],
        help_text=_('Size of a column, in percent of the total available space.')
    )
    horizontal_space = models.PositiveSmallIntegerField(
        default=3,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
        ],
        help_text=_(
            'Size of the space between the elements, in percent of the total available space.'),
    )
    row_size_px = models.PositiveSmallIntegerField(
        default=200,
        validators=[
            MinValueValidator(1)
        ],
        help_text=_('Height of each element, in pixels.')
    )
    vertical_space_px = models.PositiveSmallIntegerField(
        default=20,
        validators=[
            MinValueValidator(0)
        ],
        help_text=_(
            'Size of the vertical space between elements, in pixels.'),
    )
    stamp = models.CharField(
        default='',
        blank=True,
        max_length=128,
        help_text=_(
            'Specifies which elements are stamped within the layout.'
            'Isotope will layout items below stamped elements.'),
    )
    is_fit_width = models.BooleanField(
        default=False,
        help_text=_(
            'Sets the width of the container to fit the available number of columns, based '
            'the size of container\'s parent element. When enabled, you can center the '
            'container with CSS.'),
    )
    is_origin_left = models.BooleanField(
        default=True,
        help_text=_(
            'Controls the horizontal flow of the layout. By default, item elements '
            'start positioning at the left, with isOriginLeft: true. Set isOriginLeft: false '
            'for right-to-left layouts.'),
    )
    is_origin_top = models.BooleanField(
        default=True,
        help_text=_(
            'Controls the vertical flow of the layout. By default, item elements start '
            'positioning at the top, with isOriginTop: true. Set isOriginTop: false '
            'for bottom-up layouts. It’s like Tetris!'),
    )

    # Behaviour
    expand_on_click = models.BooleanField(
        default=False,
        help_text=_(
            'Should elements expand on click.'
        )
    )
    expand_on_click_ratio = models.PositiveSmallIntegerField(
        default=2,
        help_text=_(
            'The size of expanded elements will be multiplied by this number.'
        )
    )

    style = models.CharField(
        _('style'),
        max_length=255,
        choices=settings.DJANGOCMS_MASONRY_STYLES,
        default=settings.DJANGOCMS_MASONRY_STYLES[0][0],
        help_text=_('CSS class'), )
    template = models.CharField(
        _('template'),
        max_length=255,
        choices=settings.DJANGOCMS_MASONRY_TEMPLATES,
        default=settings.DJANGOCMS_MASONRY_TEMPLATES[0][0], )

    def get_style(self):
        if self.style and self.style != settings.DJANGOCMS_MASONRY_DEFAULT:
            return self.style
        return ''

    def get_masonry_options(self):
        options = {
            'itemSelector': '.grid-item-{}'.format(self.pk),
            'columnWidth': '.grid-sizer-{}'.format(self.pk),
            'gutter': '.gutter-sizer-{}'.format(self.pk),
            'percentPosition': True,
            'isFitwidth': self.is_fit_width,
            'isOriginLeft': self.is_origin_left,
            'isOriginTop': self.is_origin_top,
        }

        if self.extra_options:
            options.update(self.extra_options)

        return options

    def get_expanded_column_width(self):
        return self.column_size * int(self.expand_on_click_ratio)

    def get_expanded_height(self):
        return self.row_size_px * self.expand_on_click_ratio

    def get_margin_left(self):
        """
        Computes the ideal margin-left to apply to the container in order to center everything.
        This takes into account the number of columns, vertical_spaces, etc.
        """
        # Number of columns that we expect to be displayed
        number_of_columns = int(100 / self.column_size)
        # Total size of the columns + horizontal spaces
        total_size = (number_of_columns * self.column_size) + (number_of_columns - 1) * self.horizontal_space
        # If total_size > 100, this means we won't be able to have that much columns.
        # Do the computation again with one column less.
        while (total_size > 100):
            number_of_columns = number_of_columns - 1
            total_size = (number_of_columns) * self.column_size + (number_of_columns - 1) * self.horizontal_space

        # Return the space that's left once we substract total_size (%) to 100%
        return '%.2f' % ((100 - total_size))
